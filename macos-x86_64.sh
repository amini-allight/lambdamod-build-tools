#!/bin/sh
# TODO: Copy dependencies into package
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=x86_64
DISPLAY_NAME=LambdaMod
SERVER_DISPLAY_NAME="LambdaMod Server"
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-macos-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
# Install dependencies
brew install \
    git \
    cmake \
    make \
    create-dmg \
    makeicns \
    boost \
    curl \
    freetype \
    glslang \
    miniupnpc \
    msgpack-cxx \
    openssl \
    opus \
    libpng \
    vulkan-headers \
    vulkan-loader \
    websocketpp \
    libyaml
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX=/usr/local .
make -j$(sysctl -n hw.ncpu)
sudo make install
cd ..
# Build bullet
git clone --depth 1 "https://github.com/bulletphysics/bullet3"
cd bullet3
cmake -DBUILD_CPU_DEMOS=OFF -DBUILD_SHARED_LIBS=ON -DUSE_DOUBLE_PRECISION=ON -DCMAKE_INSTALL_PREFIX=/usr/local .
make -j$(sysctl -n hw.ncpu)
sudo make install
cd ..
# Build target
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
cd $PROJECT_NAME
curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
mkdir -p build
cd build
export C_INCLUDE_PATH="$C_INCLUDE_PATH:/usr/local/include"
export CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:/usr/local/include"
export LIBRARY_PATH="$LIBRARY_PATH:/usr/local/lib"
export LDFLAGS="$LDFLAGS -F/usr/local/lib"
cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF ..
make -j$(sysctl -n hw.ncpu)
cd ../..
# Construct client
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/MacOS"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Resources"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Frameworks"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "$PROJECT_NAME/bin/$PROJECT_NAME" "dmg/$DISPLAY_NAME.app/Contents/MacOS"
cat >"dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME" << EOL
#!/bin/sh
cd "\$(dirname \$0)"
export DYLD_FALLBACK_FRAMEWORK_PATH="\$DYLD_FALLBACK_FRAMEWORK_PATH:\$PWD/../Frameworks"
export DYLD_FALLBACK_LIBRARY_PATH="\$DYLD_FALLBACK_LIBRARY_PATH:\$PWD/../Libraries"
./$PROJECT_NAME
EOL
chmod +x "dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME"
cp -r "$PROJECT_NAME/bin/res" "dmg/$DISPLAY_NAME.app/Contents/MacOS"
cp "$PROJECT_NAME/license" "dmg/$DISPLAY_NAME.app/Contents/Resources"
cp -r "$PROJECT_NAME/sys/macos/licenses" "dmg/$DISPLAY_NAME.app/Contents/Resources"
makeicns -in "$PROJECT_NAME/sys/macos/AppIcon.png" -out "dmg/$DISPLAY_NAME.app/Contents/Resources/AppIcon.icns"
cp "$PROJECT_NAME/sys/macos/Info.plist" "dmg/$DISPLAY_NAME.app/Contents"
# Construct server
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS"
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources"
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/Frameworks"
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/Libraries"
cp "$PROJECT_NAME/bin/$PROJECT_NAME-server" "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS"
cat >"dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS/$SERVER_DISPLAY_NAME" << EOL
#!/bin/sh
cd "\$(dirname \$0)"
export DYLD_FALLBACK_FRAMEWORK_PATH="\$DYLD_FALLBACK_FRAMEWORK_PATH:\$PWD/../Frameworks"
export DYLD_FALLBACK_LIBRARY_PATH="\$DYLD_FALLBACK_LIBRARY_PATH:\$PWD/../Libraries"
./$PROJECT_NAME-server
EOL
chmod +x "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS/$SERVER_DISPLAY_NAME"
cp -r "$PROJECT_NAME/bin/res" "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS"
cp "$PROJECT_NAME/license" "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources"
cp -r "$PROJECT_NAME/sys/macos/licenses" "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources"
makeicns -in "$PROJECT_NAME/sys/macos/AppIcon.png" -out "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources/AppIcon.icns"
cp "$PROJECT_NAME/sys/macos/InfoServer.plist" "dmg/$SERVER_DISPLAY_NAME.app/Contents/Info.plist"
# Construct package
create-dmg \
    --volname "$DISPLAY_NAME" \
    --volicon "dmg/$DISPLAY_NAME.app/Contents/Resources/AppIcon.icns" \
    --background "$PROJECT_NAME/sys/macos/dmg-background.png" \
    --window-pos 200 120 \
    --window-size 800 400 \
    --icon-size 100 \
    --icon "$DISPLAY_NAME.app" 200 190 \
    --hide-extension "$DISPLAY_NAME.app" \
    --icon "$SERVER_DISPLAY_NAME.app" 400 190 \
    --hide-extension "$SERVER_DISPLAY_NAME.app" \
    --app-drop-link 600 185 \
    --skip-jenkins \
    "$OUTPUT_NAME.dmg" \
    "dmg"
echo "$OUTPUT_NAME.dmg" > file-name
