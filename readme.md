# LambdaMod Build Tools

Build scripts for building [LambdaMod](https://gitlab.com/amini-allight/lambdamod) using [Build Swarm](https://gitlab.com/amini-allight/build-swarm).

## License

GPL 3.0
