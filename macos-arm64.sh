#!/bin/sh
# TODO: Copy dependencies into package
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=arm64
DISPLAY_NAME=LambdaMod
SERVER_DISPLAY_NAME="LambdaMod Server"
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-macos-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
# Install dependencies
mkdir -p arm-homebrew
curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C arm-homebrew
alias arm-brew="$PWD/arm-homebrew/bin/brew"
armbrewinstall () {
    arm-brew fetch --force --bottle-tag=arm64_ventura "$1"
    arm-brew install $(arm-brew --cache --bottle-tag=arm64_ventura "$1")
}
brew install \
    git \
    cmake \
    make \
    create-dmg \
    makeicns
armbrewinstall boost
armbrewinstall curl
armbrewinstall freetype
armbrewinstall glslang
armbrewinstall miniupnpc
armbrewinstall msgpack-cxx
armbrewinstall openssl
armbrewinstall opus
armbrewinstall libpng
armbrewinstall vulkan-headers
armbrewinstall vulkan-loader
armbrewinstall websocketpp
armbrewinstall libyaml
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX=/usr/local .
make -j$(sysctl -n hw.ncpu)
sudo make install
cd ..
# Build bullet
git clone --depth 1 "https://github.com/bulletphysics/bullet3"
cd bullet3
cmake -DBUILD_CPU_DEMOS=OFF -DBUILD_SHARED_LIBS=ON -DUSE_DOUBLE_PRECISION=ON -DCMAKE_INSTALL_PREFIX=/usr/local .
make -j$(sysctl -n hw.ncpu)
sudo make install
cd ..
# Build target
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
cd $PROJECT_NAME
curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
mkdir -p build
cd build
export C_INCLUDE_PATH="$PWD/arm-homebrew/include:$C_INCLUDE_PATH"
export CPLUS_INCLUDE_PATH="$PWD/arm-homebrew/include:$CPLUS_INCLUDE_PATH"
export LIBRARY_PATH="$PWD/arm-homebrew/lib:$LIBARY_PATH"
export LDFLAGS="$LDFLAGS -F$PWD/arm-homebrew/lib"
export CFLAGS="$CFLAGS -target arm64-apple-darwin22.6.0"
export CXXFLAGS="$CXXFLAGS -target arm64-apple-darwin22.6.0"
cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF ..
make -j$(sysctl -n hw.ncpu)
cd ../..
# Construct client
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/MacOS"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Resources"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Frameworks"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "$PROJECT_NAME/bin/$PROJECT_NAME" "dmg/$DISPLAY_NAME.app/Contents/MacOS"
cat >"dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME" << EOL
#!/bin/sh
cd "\$(dirname \$0)"
export DYLD_FALLBACK_FRAMEWORK_PATH="\$DYLD_FALLBACK_FRAMEWORK_PATH:\$PWD/../Frameworks"
export DYLD_FALLBACK_LIBRARY_PATH="\$DYLD_FALLBACK_LIBRARY_PATH:\$PWD/../Libraries"
./$PROJECT_NAME
EOL
chmod +x "dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME"
cp -r "$PROJECT_NAME/bin/res" "dmg/$DISPLAY_NAME.app/Contents/MacOS"
cp "$PROJECT_NAME/license" "dmg/$DISPLAY_NAME.app/Contents/Resources"
cp -r "$PROJECT_NAME/sys/macos/licenses" "dmg/$DISPLAY_NAME.app/Contents/Resources"
makeicns -in "$PROJECT_NAME/sys/macos/AppIcon.png" -out "dmg/$DISPLAY_NAME.app/Contents/Resources/AppIcon.icns"
cp "$PROJECT_NAME/sys/macos/Info.plist" "dmg/$DISPLAY_NAME.app/Contents"
# Construct server
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS"
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources"
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/Frameworks"
mkdir -p "dmg/$SERVER_DISPLAY_NAME.app/Contents/Libraries"
cp "$PROJECT_NAME/bin/$PROJECT_NAME-server" "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS"
cat >"dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS/$SERVER_DISPLAY_NAME" << EOL
#!/bin/sh
cd "\$(dirname \$0)"
export DYLD_FALLBACK_FRAMEWORK_PATH="\$DYLD_FALLBACK_FRAMEWORK_PATH:\$PWD/../Frameworks"
export DYLD_FALLBACK_LIBRARY_PATH="\$DYLD_FALLBACK_LIBRARY_PATH:\$PWD/../Libraries"
./$PROJECT_NAME-server
EOL
chmod +x "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS/$SERVER_DISPLAY_NAME"
cp -r "$PROJECT_NAME/bin/res" "dmg/$SERVER_DISPLAY_NAME.app/Contents/MacOS"
cp "$PROJECT_NAME/license" "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources"
cp -r "$PROJECT_NAME/sys/macos/licenses" "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources"
makeicns -in "$PROJECT_NAME/sys/macos/AppIcon.png" -out "dmg/$SERVER_DISPLAY_NAME.app/Contents/Resources/AppIcon.icns"
cp "$PROJECT_NAME/sys/macos/InfoServer.plist" "dmg/$SERVER_DISPLAY_NAME.app/Contents/Info.plist"
# Construct package
create-dmg \
    --volname "$DISPLAY_NAME" \
    --volicon "dmg/$DISPLAY_NAME.app/Contents/Resources/AppIcon.icns" \
    --background "$PROJECT_NAME/sys/macos/dmg-background.png" \
    --window-pos 200 120 \
    --window-size 800 400 \
    --icon-size 100 \
    --icon "$DISPLAY_NAME.app" 200 190 \
    --hide-extension "$DISPLAY_NAME.app" \
    --icon "$SERVER_DISPLAY_NAME.app" 400 190 \
    --hide-extension "$SERVER_DISPLAY_NAME.app" \
    --app-drop-link 600 185 \
    --skip-jenkins \
    "$OUTPUT_NAME.dmg" \
    "dmg"
echo "$OUTPUT_NAME.dmg" > file-name
