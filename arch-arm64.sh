#!/bin/sh
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=arm64
PROJECT_ARCHITECTURE_ARCH=aarch64
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-arch-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
sudo pacman -Sy git cmake clang make
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg.git"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX=/usr .
make -j$(nproc)
sudo make install
cd ..
# Build target
mkdir $PROJECT_NAME
cat >"$PROJECT_NAME/PKGBUILD" << EOL
pkgname=$PROJECT_NAME
pkgver=$BUILD_SWARM_PROGRAM_VERSION
pkgrel=$BUILD_SWARM_RELEASE_NUMBER
pkgdesc="An experimental multiplayer sandbox game with optional VR support."
arch=('$PROJECT_ARCHITECTURE_ARCH')
url="https://gitlab.com/amini-allight/$PROJECT_NAME"
license=('GPL3')
makedepends=('git' 'cmake' 'clang' 'make' 'boost' 'glslang' 'msgpack-cxx' 'vulkan-headers' 'websocketpp')
depends=('bullet-dp' 'curl' 'freetype2' 'miniupnpc' 'openssl' 'openxr' 'opus' 'libpng' 'libpulse' 'vulkan-driver' 'vulkan-icd-loader' 'libx11' 'libxrender' 'libxt' 'libyaml')
provides=("$PROJECT_NAME")
conflicts=("$PROJECT_NAME")
source=("git+https://gitlab.com/amini-allight/$PROJECT_NAME.git")
md5sums=('SKIP')

prepare() {
    cd $PROJECT_NAME
    git pull origin master
}

build() {
    cd $PROJECT_NAME
    rm -rf build
    curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
    mkdir -p build
    cd build
    cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF -DCMAKE_INSTALL_PREFIX=/usr ..
    make -j\$(nproc)
}

package() {
    cd $PROJECT_NAME
    cd build
    make DESTDIR="\$pkgdir/" install
}
EOL
cd "$PROJECT_NAME"
makepkg -sr --noconfirm
cd ..
mv "$PROJECT_NAME/$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-$PROJECT_ARCHITECTURE_ARCH.pkg.tar.xz" "$OUTPUT_NAME.pkg.tar.xz"
echo "$OUTPUT_NAME.pkg.tar.xz" > file-name
