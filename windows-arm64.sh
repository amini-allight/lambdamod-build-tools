#!/bin/sh
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=arm64
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-windows-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
export CMAKE_GENERATOR="Unix Makefiles"
export CFLAGS="$CFLAGS -DWIN32"
export CXXFLAGS="$CXXFLAGS -DWIN32"
# Install dependencies
pacman -S --noconfirm \
    git \
    $MINGW_PACKAGE_PREFIX-clang \
    $MINGW_PACKAGE_PREFIX-cmake \
    make \
    p7zip \
    $MINGW_PACKAGE_PREFIX-boost \
    $MINGW_PACKAGE_PREFIX-bullet-dp \
    $MINGW_PACKAGE_PREFIX-curl \
    $MINGW_PACKAGE_PREFIX-freetype \
    $MINGW_PACKAGE_PREFIX-glslang \
    $MINGW_PACKAGE_PREFIX-miniupnpc \
    $MINGW_PACKAGE_PREFIX-openssl \
    $MINGW_PACKAGE_PREFIX-openxr-sdk \
    $MINGW_PACKAGE_PREFIX-opus \
    $MINGW_PACKAGE_PREFIX-libpng \
    $MINGW_PACKAGE_PREFIX-librsvg \
    $MINGW_PACKAGE_PREFIX-vulkan-headers \
    $MINGW_PACKAGE_PREFIX-vulkan-loader \
    $MINGW_PACKAGE_PREFIX-libyaml
# Install Websocket++
rm -rf "$MINGW_PREFIX/include/websocketpp"
git clone --depth 1 "https://github.com/zaphoyd/websocketpp"
cp -r "websocketpp/websocketpp" "$MINGW_PREFIX/include"
# Install msgpack-cxx
rm -rf "$MINGW_PREFIX/include/msgpack"
rm -f "$MINGW_PREFIX/include/msgpack.hpp"
git clone --depth 1 -b cpp_master "https://github.com/msgpack/msgpack-c"
cp -r "msgpack-c/include/"* "$MINGW_PREFIX/include"
sed -i "s/typedef unsigned char byte;//g" "$MINGW_PREFIX/include/rpcndr.h"
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX="$MINGW_PREFIX" .
make -j$(nproc)
make install
cd ..
# Build target
git clone --depth 1 "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
cd $PROJECT_NAME
git pull origin master
curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
mkdir -p build
cd build
cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF ..
make -j$(nproc)
cd ../..
# Construct package
mkdir -p "$OUTPUT_NAME"
cp "$PROJECT_NAME/bin/$PROJECT_NAME.exe" "$OUTPUT_NAME"
cp "$PROJECT_NAME/bin/$PROJECT_NAME-server.exe" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libbrotlicommon.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libbrotlidec.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libbz2-1.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libBulletCollision.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libBulletDynamics.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libcrypto-3.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libcurl-4.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libfreetype-6.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libgcc_s_seh-1.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libglib-2.0-0.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libgraphite2.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libharfbuzz-0.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libiconv-2.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libidn2-0.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libintl-8.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libLinearMath.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libminiupnpc.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libnghttp2-14.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libopenxr_loader.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libopus-0.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libpcre2-8-0.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libpng16-16.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libpsl-5.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libssh2-1.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libssl-3.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libstdc++-6.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libunistring-5.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/vulkan-1.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libwinpthread-1.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libyaml-0-2.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/zlib1.dll" "$OUTPUT_NAME"
cp "$MINGW_PREFIX/bin/libzstd.dll" "$OUTPUT_NAME"
cp -r "$PROJECT_NAME/bin/res" "$OUTPUT_NAME"
cp "$PROJECT_NAME/license" "$OUTPUT_NAME"
cp -r "$PROJECT_NAME/sys/windows/licenses" "$OUTPUT_NAME"
7z a "$OUTPUT_NAME.zip" "$OUTPUT_NAME"
echo "$OUTPUT_NAME.zip" > file-name
