#!/bin/sh
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=x86_64
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-rhel-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
# Install dependencies
sudo yum install -y rpmdevtools yum-utils git make cmake clang
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg.git"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX=/usr .
make -j$(nproc)
sudo make install
cd ..
# Build bullet
git clone --depth 1 "https://github.com/bulletphysics/bullet3"
cd bullet3
cmake -DBUILD_CPU_DEMOS=OFF -DBUILD_SHARED_LIBS=OFF -DUSE_DOUBLE_PRECISION=ON -DCMAKE_INSTALL_PREFIX=/usr .
make -j$(nproc)
sudo make install
cd ..
# Build target
rm -rf "$HOME/rpmbuild"
rpmdev-setuptree
cat >"$HOME/rpmbuild/SPECS/$PROJECT_NAME.spec" << EOL
Name:           $PROJECT_NAME
Version:        $BUILD_SWARM_PROGRAM_VERSION
Release:        $BUILD_SWARM_RELEASE_NUMBER
Summary:        An experimental multiplayer sandbox game with optional VR support.
License:        GPLv3+
URL:            https://amini-allight.org/$PROJECT_NAME
Source0:        https://gitlab.com/amini-allight/$PROJECT_NAME
BuildArch:      $PROJECT_ARCHITECTURE
BuildRequires:  git
BuildRequires:  clang
BuildRequires:  cmake
BuildRequires:  make
BuildRequires:  boost-devel
BuildRequires:  libcurl-devel
BuildRequires:  freetype-devel
BuildRequires:  glslang
BuildRequires:  miniupnpc-devel
BuildRequires:  msgpack-devel
BuildRequires:  openssl-devel
BuildRequires:  openxr-devel
BuildRequires:  opus-devel
BuildRequires:  libpng-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  vulkan-headers
BuildRequires:  vulkan-loader-devel
BuildRequires:  websocketpp-devel
BuildRequires:  libX11-devel
BuildRequires:  libXrender-devel
BuildRequires:  libXt-devel
BuildRequires:  libyaml-devel
Requires: libcurl
Requires: freetype
Requires: miniupnpc
Requires: openssl
Requires: openxr
Requires: opus
Requires: libpng
Requires: pulseaudio-libs
Requires: mesa-vulkan-drivers
Requires: libX11
Requires: libXrender
Requires: libXt
Requires: libyaml

%description
An experimental multiplayer sandbox game with optional VR support.

%prep
rm -rf $PROJECT_NAME
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME"
cd $PROJECT_NAME

%build
cd $PROJECT_NAME
curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
mkdir -p build
cd build
cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF -DCMAKE_INSTALL_PREFIX=/usr ..
make -j\$(nproc)

%install
cd $PROJECT_NAME/build
make DESTDIR=%{buildroot} install
cd %{buildroot}
find . -type f | cut -c 2- > "$HOME/rpmbuild/BUILD/file-list"

%files -f file-list

%changelog
# empty
EOL
sudo yum-builddep -y "$HOME/rpmbuild/SPECS/$PROJECT_NAME.spec"
rpmbuild -bb "$HOME/rpmbuild/SPECS/$PROJECT_NAME.spec"
mv "$HOME/rpmbuild/RPMS/$PROJECT_ARCHITECTURE/$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION.$PROJECT_ARCHITECTURE.rpm" "$OUTPUT_NAME.rpm"
echo "$OUTPUT_NAME.rpm" > file-name
