#!/usr/local/bin/bash
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=x86_64
PROJECT_ARCHITECTURE_FREEBSD=amd64
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-freebsd-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
# Install dependencies
sudo pkg install -y \
    git \
    wget \
    llvm-devel \
    cmake \
    boost-all \
    bullet-double \
    curl \
    freetype2 \
    glslang \
    miniupnpc \
    msgpack-cxx \
    opus \
    png \
    pulseaudio \
    vulkan-headers \
    vulkan-loader \
    websocketpp \
    libx11 \
    libxrender \
    libxt \
    libyaml
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX=/usr/local .
make -j$(nproc)
sudo make install
cd ..
# Build target
git clone --depth 1 "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
mkdir -p package
cd $PROJECT_NAME
curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
mkdir -p build
cd build
export C_INCLUDE_PATH="$C_INCLUDE_PATH:/usr/local/include"
export CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:/usr/local/include"
export LIBRARY_PATH="$LIBRARY_PATH:/usr/local/lib"
cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j$(nproc)
make DESTDIR="../../package" install
cd ../..
# Construct package
mkdir -p metadata
cat >metadata/+MANIFEST <<EOL
name: "$PROJECT_NAME"
version: "$BUILD_SWARM_PACKAGE_VERSION"
origin: "games/$PROJECT_NAME"
comment: "An experimental multiplayer sandbox game with optional VR support."
desc: "An experimental multiplayer sandbox game with optional VR support."
arch: "$PROJECT_ARCHITECTURE_FREEBSD"
maintainer: "amini.allight@protonmail.com"
www: "https://amini-allight.org/$PROJECT_NAME"
prefix: "/usr/local"
deps: {
    "bullet-double": {
        "version": "3.25",
        "origin": "devel/bullet-double"
    },
    "curl": {
        "version": "8.5.0",
        "origin": "ftp/curl"
    },
    "freetype2": {
        "version": "2.13.2",
        "origin": "print/freetype2"
    },
    "miniupnpc": {
        "version": "2.2.5",
        "origin": "net/miniupnpc"
    },
    "opus": {
        "version": "1.4",
        "origin": "audio/opus"
    },
    "png": {
        "version": "1.6.40",
        "origin": "graphics/png"
    },
    "pulseaudio": {
        "version": "16.1",
        "origin": "audio/pulseaudio"
    },
    "vulkan-loader": {
        "version": "1.2.0",
        "origin": "graphics/vulkan-loader"
    },
    "libx11": {
        "version": "1.8.7",
        "origin": "x11/libx11"
    },
    "libxrender": {
        "version": "0.9.10",
        "origin": "x11/libxrender"
    },
    "libxt": {
        "version": "1.2.1",
        "origin": "x11-toolkits/libxt"
    },
    "libyaml": {
        "version": "0.2.5",
        "origin": "textproc/libyaml"
    }
}
files: {
EOL
while IFS= read -r line
do
    FILE_HASH=($(sha256sum "$line"))
    echo "    \"${line#package}\": \"$HASH_RESULT\"," >> metadata/+MANIFEST
done <<< $(find package -type f)
echo "}" >> metadata/+MANIFEST
pkg create -r package -m metadata
mv "$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION.pkg" "$OUTPUT_NAME.pkg"
echo "$OUTPUT_NAME.pkg" > file-name
