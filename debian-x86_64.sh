#!/bin/bash
PROJECT_NAME=lambdamod
PROJECT_ARCHITECTURE=x86_64
PROJECT_ARCHITECTURE_DEBIAN=amd64
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-debian-$PROJECT_ARCHITECTURE
export CC=clang
export CXX=clang++
# Install dependencies
yes | sudo add-apt-repository universe
sudo apt update
sudo apt install -y \
    git \
    clang \
    cmake \
    libcurl4-openssl-dev \
    libfreetype-dev \
    glslang-tools \
    libminiupnpc-dev \
    libmsgpack-dev \
    libopenxr-dev \
    libopus-dev \
    libpng-dev \
    libpulse-dev \
    libssl-dev \
    libvulkan-dev \
    libwebsocketpp-dev \
    libx11-dev \
    libxrender-dev \
    libxt-dev \
    libyaml-dev
# Build lunasvg
git clone --depth 1 "https://github.com/sammycage/lunasvg"
cd lunasvg
cmake -DCMAKE_INSTALL_PREFIX=/usr .
make -j$(nproc)
sudo make install
cd ..
# Build bullet
git clone --depth 1 "https://github.com/bulletphysics/bullet3"
cd bullet3
cmake -DBUILD_CPU_DEMOS=OFF -DBUILD_SHARED_LIBS=OFF -DUSE_DOUBLE_PRECISION=ON -DCMAKE_INSTALL_PREFIX=/usr .
make -j$(nproc)
sudo make install
cd ..
# Build target
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git" target
mkdir -p "$PROJECT_NAME/DEBIAN"
cd target
curl -s -o src/libraries/vk_mem_alloc/vk_mem_alloc.h "https://raw.githubusercontent.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/master/include/vk_mem_alloc.h"
mkdir -p build
cd build
cmake -DLMOD_DEPLOYMENT=$BUILD_SWARM_DEPLOY -DLMOD_PERSONAL=OFF -DCMAKE_INSTALL_PREFIX=/usr ..
make -j$(nproc)
make DESTDIR="../../$PROJECT_NAME" install
cd ../..
# Construct package
cat >"$PROJECT_NAME/DEBIAN/control" <<EOL
Package: $PROJECT_NAME
Version: $BUILD_SWARM_PACKAGE_VERSION
Section: games
Priority: optional
Architecture: $PROJECT_ARCHITECTURE_DEBIAN
Maintainer: Amini Allight <amini.allight@protonmail.com>
Homepage: https://amini-allight.org/$PROJECT_NAME
Description: An experimental multiplayer sandbox game with optional VR support.
Depends: curl (>= 7.88), libfreetype6 (>= 2.12.1), miniupnpc (>= 2.2.4), openssl (>= 3.0.8), libopenxr-loader1 (>= 1.0.20), libopus0 (>= 1.3.1), libpng16-16 (>= 1.6.39), libpulse0 (>= 16.1), libvulkan1 (>= 1.2.0), libx11-6 (>= 1.8.4), libxrender1 (>= 0.9.10), libxt6 (>= 1.2.1), libyaml-0-2 (>= 0.2.5)
EOL
dpkg-deb --root-owner-group --build $PROJECT_NAME
mv "$PROJECT_NAME.deb" "$OUTPUT_NAME.deb"
echo "$OUTPUT_NAME.deb" > file-name
